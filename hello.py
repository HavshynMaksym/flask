from flask import Flask
from faker import Faker
import csv
fake = Faker()

app = Flask(__name__)



def average_data():
    f = open('hw.csv')

    weights = []
    heights = []
    read_csv = csv.reader(f, delimiter=',')
    for row in read_csv:
            weight = float(row[2])
            height = float(row[1])
            weights.append(weight)
            heights.append(height)

    total_weight = sum(weights)
    POUNDS_PER_KG = 0.45359237
    average_weights = round(((total_weight / len(weights) * POUNDS_PER_KG)), 2)

    total_heights = sum(heights)
    INCHES_PER_CM = 2.54
    average_heights = round(((total_heights / len(heights) * INCHES_PER_CM)), 2)

    return (f'Средний вес = {average_weights} кг <p> Средний рост = {average_heights} см</p>')



def random_name():
    compound = []
    for i in range(1, 101):
        name_and_mail = []
        name_and_mail.append(fake.name())
        name_and_mail.append(fake.ascii_email())
        compound.append(name_and_mail)
    return tuple(compound)


def display_random_name():
    names = dict(random_name())

    display = ''
    for i in range(1, 101):
        for key in names:
            text = (f'<p>Name: {key}, email: {names[key]}</p>')
            display += text
    return display

@app.route('/')
def hello():
    return "hello, world"


@app.route('/requirements')
def get_requirements():
    f = open('requirements.txt')
    return f.read()


@app.route('/random_users')
def get_random_users():
    return display_random_name()


@app.route('/avr_data')
def get_avr_data():
    return average_data()


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8003)
